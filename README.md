# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is a basic USSD application written with Flask. Using Africa's Talking USSD API's


## Version ##
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

This is a Python 3 application.

Dependencies:
* Flask
* Python 3

```pip3 install flask```

How to run the application:

``` python3 ussd_urls_routing.py ```

#### How to create a new USSD JOURNEY: ####
* Create a new json file e.g. hello_world.json
* Populate the hello_world.json ... Example below:

```
{
    "home": {
        "screen_msg": "What is your age\n1. 0-19 yrs\n2. 20-35 yrs\n3. 36-50 yrs\n4. 51+ yrs",
        "next_options": {
            "else": "games"
        }
    },
    "games": {
        "screen_msg": "Are you a gamer?\n1. Yes\n2. No",
        "next_options": {
            "1": "gamers",
            "2": "non_gamers"
        }
    },
    "non_gamers": {
        "screen_msg": "The cake is not a lie",
        "next_options": {}
    },
    "gamers": {
        "screen_msg": "Choose your gaming console\n1. PC\n 2. PS4\n3. Xbox",
        "next_options": {
            "1": "pc_gamers",
            "2": "ps4_gamers",
            "3": "xbox_gamers"
        }
    },
    "pc_gamers": {
        "screen_msg": "Mount and Blade 2 is your best upcoming exclusive",
        "next_options": {}
    },
    "ps4_gamers": {
        "screen_msg": "God of War is your best upcoming exclusive",
        "next_options": {}
    },
    "xbox_gamers": {
        "screen_msg": "State of Decay 2 is your best upcoming exclusive",
        "next_options": {}
    }
}
```
* Implement the new journey:
```python3 ussd_generator.py --i home -a create_package -j /path/to/hello_world.json -u hello```

* Restart the service: <first kill the service e.g. Ctrl+C or 'kill <pid>'> then start the service:
```
python3 ussd_urls_routing.py
```

### URL requests ###

```

POST /ussd/at.php HTTP/1.1
Host: 139.59.166.134:7771
User-Agent: akka-http/10.0.4
Content-Type: application/x-www-form-urlencoded; charset=UTF-8
Content-Length: 109

sessionId=fa539d7a-95ae-422b-9937-e5d5ce7e4dd4&serviceCode=%2A384%2A191%23&phoneNumber=%2B254722550150&text=1HTTP/1.1 200 OK
Date: Fri, 02 Jun 2017 07:30:31 GMT
Server: Apache/2.4.18 (Ubuntu)
Vary: Accept-Encoding
Content-Length: 86
Content-Type: text/plain;charset=UTF-8

CON Choose account information you want to view 
1. Account number 
2. Account balancePOST /ussd/at.php HTTP/1.1
Host: 139.59.166.134:7771
User-Agent: akka-http/10.0.4
Content-Type: application/x-www-form-urlencoded; charset=UTF-8
Content-Length: 113

sessionId=fa539d7a-95ae-422b-9937-e5d5ce7e4dd4&serviceCode=%2A384%2A191%23&phoneNumber=%2B254722550150&text=1%2A2HTTP/1.1 200 OK
Date: Fri, 02 Jun 2017 07:30:34 GMT
Server: Apache/2.4.18 (Ubuntu)
Content-Length: 30
Content-Type: text/plain;charset=UTF-8

END Your balance is KES 10,000
```



### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### Need to do ###
* Create option to hook up session_dict to database(s)
* Provide an example on how to:
*  * introduce logic that changes the screen to render
*  * manupulating the current_session['data'] dict
* Include screen_msg length (character count) in the session's dict
* gunicorn / apache hookup via wsgi - how to deploy