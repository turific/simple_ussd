''' This is the state machine '''
from ussd_core import screen_renderer

SETTINGS_DEFAULT_LANG = 'sw' # A cleaner way of doing this can be implemented e.g. using an actual settings file

def session_initiator(user_input, current_session):
    current_session['class_name'] = 'home'
    current_session['data']['language'] = 'en'  # This is the kind of detail picked from the USSD-GW / lookup to get what the customer's preferred language is
    current_session['language']= {'default': SETTINGS_DEFAULT_LANG}
    return home.pre(current_session)


class home():
    @staticmethod
    @screen_renderer
    def pre(current_session):
        return None

    @staticmethod
    def post(current_session):
        return quit.__name__


class engineering():
    @staticmethod
    @screen_renderer
    def pre(current_session):
        return None


class people():
    @staticmethod
    @screen_renderer
    def pre(current_session):
        return None


class it():
    @staticmethod
    @screen_renderer
    def pre(current_session):
        return None


class quit():
    @staticmethod
    @screen_renderer
    def pre(current_session):
        return None
