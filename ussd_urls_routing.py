from flask import Flask
from flask import request, jsonify

from ussd_core import st_mach_controller, at_fetch_input_and_session, ussd_routed_req
from ussd_generator import add_ussd_journey_from_api

import uce.st_machine
import poll1.st_machine


app = Flask(__name__)


@app.route('/at/add/<package_name>', methods=['POST'])
def route_add_json(package_name):
    add_ussd_journey_from_api(request.data, package_name)
    return jsonify({'a': "aaa"})



@app.route('/at', methods=['POST', 'GET'])
def route_at_whitelisting():

    # handle the shortcode routing extension
    user_input, current_session = at_fetch_input_and_session(request, "at_long")



    # Whitelisting functionality
    if request.form['phoneNumber'] in ['+254722550xxx', '+254722111222', '+254792695085', '+254700000027',
                                       '+254712707080', '+254711222333xx']:
        return ussd_at_uce(current_session)
    elif request.form['phoneNumber'] in ['+254722333444', '+254722555777']:
        return ussd_at_poll1(current_session)

    if current_session['initial_screen'] == 'False':
        print ("current_session['transition'][0]['user_input'] = " + current_session['transition'][0]['user_input'])

    # Routing functionality
    if (current_session['initial_screen'] == 'True' and len(user_input) > 0) \
            or (current_session['initial_screen'] == 'False' and current_session['transition'][0]['user_input'] != ''):
        current_session['journey_id'] = user_input if current_session['initial_screen'] == 'True' else current_session['journey_id']
        journey_id = current_session['journey_id']
        print ("current_session['initial_screen'] ==== " + current_session['initial_screen'])
        return sc_ext[journey_id](current_session)

    else:
        # force presenting the UCE project
        return ussd_at_uce(current_session)
        # return "END This is the home page.\nDial again with a short-code\nExample:\n*483*27*2#"


@app.route('/at/long/uce', methods=['POST', 'GET'])
def route_ussd_at_uce():
    user_input, current_session = at_fetch_input_and_session(request, 'at_long')
    return ussd_at_uce(current_session)


@ussd_routed_req
def ussd_at_uce(current_session):
    return st_mach_controller(current_session['user_input'], current_session, uce.st_machine)



@app.route('/at/long/poll1', methods=['POST', 'GET'])
def route_ussd_at_poll1():
    user_input, current_session = at_fetch_input_and_session(request, 'at_long')
    return ussd_at_poll1(current_session)


@ussd_routed_req
def ussd_at_poll1(current_session):
    return st_mach_controller(current_session['user_input'], current_session, poll1.st_machine)


sc_ext = {'1': ussd_at_uce, '2': ussd_at_poll1}

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5001)
