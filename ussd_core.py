import datetime, os, sys
from ussd_generator import get_ussd_dict_from_file
from copy import deepcopy
from functools import wraps

session_dict = {'0722':{'123':
                        {'msisdn':'0722',
                         'session_id':'123',
                         'screen_name': "list_hospitals",
                         'session_start_time':"2017-06-12 06:14:22.3863",
                         'next_options':{},
                         'data':{'last_text': '1*3*4', 'user': "pmmoturi", 'blood_type': "B+"},
                         'transition':[]}}}


def at_get_user_input(text,current_session,ussd_gw):
    if ussd_gw == "at_long":
        len_of_prev_text = len(current_session['text'])

        if len(current_session['transition']) == 1 and current_session['transition'][0]['user_input'] != '':
            len_of_prev_text += 1
        elif len(current_session['transition']) > 1:
            len_of_prev_text += 1

        return text[len_of_prev_text:]
    elif ussd_gw == "at_short":
        return text


def at_fetch_input_and_session(request, ussd_gw):
    msisdn = request.form['phoneNumber']
    session_id = request.form['sessionId']
    service_code = request.form['serviceCode']
    user_input = ""
    text = request.form['text']
    if text == "":
        text = ""
        user_input = ""

    # identify which session to use
    current_session = dict()
    if msisdn in session_dict:
        if session_id in session_dict[msisdn]:
            current_session = deepcopy(session_dict[msisdn][session_id])
            current_session["initial_screen"] = "False"
            # print "PREVIOUS_session:\n" + str(current_session)

            user_input = at_get_user_input(text, current_session, ussd_gw)

            # update the last_input field in the data dict before 'user_input' is over written by the users' current input
            current_session['data']['last_text'] = current_session['user_input']

            current_session['text'] = text
            current_session['user_input'] = user_input
            session_dict[msisdn][session_id] = current_session
        else:
            user_input = text
            current_session = create_session(msisdn, session_id, service_code, text, user_input)
    else:
        user_input = text
        current_session = create_session(msisdn, session_id, service_code, text, user_input)

    current_session['ussd_gw'] = ussd_gw

    print ("current_session:\n" + str(current_session))
    return user_input, current_session


def prepend_at(screen_msg,next_options):
    if screen_msg.startswith("CON ") or screen_msg.startswith("END "):
        return screen_msg
    else:
        if len(next_options) > 0:
            return "CON " + screen_msg
        else:
            return "END " + screen_msg


def add_transition():
    pass


def delete_session(msisdn,session_id):
    print ("deleting session_id = " + session_id)
    del session_dict[msisdn][session_id]


def check_session(msisdn,session_id):
    try:
        print ("looking for session_id = " + session_id)
        session_info = session_dict[msisdn][session_id]
        return session_info
    except Exception as e:
        print (e)
        print ("couldn't find it")
        return False


def create_session(msisdn,session_id, service_code, text, user_input):
    try:

        current_session = {'screen_name': "",
                           'msisdn': msisdn,
                           'initial_screen': "True",
                           'service_code': service_code,
                           'text':text,
                           'user_input': user_input,
                           'session_id': session_id,
                           'session_start_time':str(datetime.datetime.now()),
                           'data': {'msisdn': msisdn, 'last_text': '', 'user': "", 'blood_type': ""},
                           'transition':[]}
        if msisdn in session_dict:
            session_dict[msisdn][session_id] = current_session
        else:
            session_dict[msisdn] = {session_id: current_session}

        # execute a lookup to get details about the customer
        #current_session['data'] = cust_lookup_loans(msisdn)

        print (session_dict)
        return current_session

    except Exception as e:
        print (e)


def post_processing(current_session):
    temp_session_info = deepcopy(current_session)

    key_name = current_session['transition'][-1]['screen_name']
    key_value = current_session['user_input']

    temp_session_info['data'][key_name] = key_value

    # print (key_name + " = " + key_value)
    # print ("temp session = " + str(temp_session_info))

    last_screen_name = key_name
    temp_session_info['last_screen_name'] = last_screen_name

    return temp_session_info


def st_mach_controller(user_input, current_session, st_machine_selected):
    if current_session["initial_screen"] == "True":

        package_dir = os.path.dirname(st_machine_selected.__file__)

        os.chdir(package_dir)
        ussd_journey_text = get_ussd_dict_from_file("ussd_journey.json")

        current_session['journey_text'] = ussd_journey_text

        # initiate the session
        st_machine_selected.session_initiator(current_session)

        # identify the initial screen:
        for key,value in ussd_journey_text.items():
            if value.get("type", None) == "initial":
                class_name = key
                current_session['class_name'] = class_name
                current_session['rerouted_class'] = class_name

                class_obj = getattr(st_machine_selected, class_name)
                return class_obj.pre(current_session)


    else:
        # append user input as the value of the last screen's name
        current_session1 = post_processing(current_session)
        class_name = None

        # gives the ability to use variables (dynamic) next options
        for key, value in current_session["next_options"].items():
            if user_input == key.format(**current_session['data']):
                class_name = current_session["next_options"][key].format(back=current_session["last_screen_name"])

        # identify where to transition  to
        if user_input in current_session["next_options"]:
            # Go to the name of the class that's the value of the key!
            class_name = current_session["next_options"][user_input]
        else:
            # Go to the name of the class that's the value of the key!
            class_name = current_session["next_options"]["else"]

        current_session1['class_name'] = class_name
        current_session1['rerouted_class'] = class_name
        print ("assigning rerouted_class" + class_name)
        class_obj = getattr(st_machine_selected, class_name)
        return class_obj.pre(current_session1)


def render_finally(current_session, screen_name, screen_msg, next_options):
    if current_session['ussd_gw'] in ("at_long", "at_short"):
        screen_msg = prepend_at(screen_msg, next_options)
    # Update the session_dict - next_options, timestamp_rendered (transition_array), Screen_text (transition array)
    update_after_render(current_session, current_session['msisdn'], current_session['session_id'],
                                  screen_name, screen_msg, next_options)
    print (screen_msg)
    return screen_msg


def update_after_render (current_session, msisdn, session_id, screen_name, screen_msg, next_options) :
    assert (msisdn is not None), "msisdn not passed!"

    temp_current_session = deepcopy(current_session)
    temp_session_info = deepcopy(session_dict[msisdn][session_id])
    temp_current_session['next_options'] = next_options
    temp_current_session['screen_name'] = screen_name
    temp_current_session['transition'].append({'time_stamp': str(datetime.datetime.now()),
                                               'user_input': temp_session_info['user_input'],
                                               'text_rendered': screen_msg,
                                               'screen_name': screen_name})
    temp_session_info = temp_current_session
    session_dict[msisdn][session_id] = temp_session_info


def screen_renderer(func):

    @wraps(func)
    def wrapper(current_session):
        next_screen = None
        processing_class = current_session['rerouted_class']

        module = sys.modules[func.__module__]
        print ("Module: " + module.__name__)
        # execute the "post_processing" function in the previous screen's.
        # if it returns None i.e.

        if current_session["initial_screen"] == "False":
            # only execute post processing of "last rendered" screen
            # i.e. a screen that was actually rendered to the customer
            print ("point A")
            print ("current_session['rerouted_class']: "+ current_session['rerouted_class'])
            print ("current_session['class_name']: " + current_session['class_name'])
            if current_session['rerouted_class'] == current_session['class_name']:
                print ("point B")
                prev_class = getattr(module, current_session['last_screen_name'])
                print ("prev class:" + prev_class.__name__)
                if hasattr(prev_class, 'post'):
                    print ("executing post processing of: " + prev_class.__name__)
                    next_screen = prev_class.post(current_session)

        if next_screen is None:
            print ("executing pre processing! of " + processing_class)
            next_screen = func(current_session)

        print ("NEXT SCREEN === " + str(next_screen))
        if next_screen is not None:
            print ("transitioning to diff screen: " + str(next_screen))
            screen_name = next_screen
            #current_session['last_screen_name'] = current_session['rerouted_class']  # last_screen_name = 'national_id' & rerouted_class = 'gender'
            current_session['rerouted_class'] = screen_name
            print ("current_session['rerouted_class'] ==== " + str(current_session['rerouted_class']))
            re_routed_class = getattr(module, current_session['rerouted_class'])
            screen_name = re_routed_class.pre(current_session) or screen_name
            print ("screen_name ==== " + str(screen_name))

        else:
            screen_name = current_session['class_name']

        if processing_class != current_session['class_name']:
            print ("returning ------> " + current_session['rerouted_class'])
            screen_name = current_session['rerouted_class']
            return current_session['rerouted_class']

        # adding translations logic here:
        if type(current_session['journey_text'][screen_name]['screen_msg']) is dict:
            lang = current_session['data'].get('language', 'default')

            if lang == 'default':
                lang = current_session['language']['default']

            screen_msg_temp = current_session['journey_text'][screen_name]['screen_msg'][lang]
        else:
            screen_msg_temp = current_session['journey_text'][screen_name]['screen_msg']

        next_options = current_session['journey_text'][screen_name]['next_options']

        if 'else' not in next_options:
            next_options['else'] = screen_name
        screen_msg = screen_msg_temp.format(**current_session['data'])
        print ("!!! rending finally! ")
        return render_finally(current_session, screen_name, screen_msg, next_options)
    return wrapper


def ussd_routed_req(func):
    def wrapper(current_session):
        error = "END There seems to have been a TURI system error. Please try again in a short while"
        try:
            ussd_response = func(current_session)
            return ussd_response

        except Exception as e:
            print (e)
            return error

    return wrapper


if __name__ == "__main__" :
    print (check_session('0722','123'))
    delete_session('0722','123')
    print (session_dict)
    create_session('0722','123')
    create_session('0722','333')