import sys, string, json, os, getopt, os.path


class CodeGeneratorBackend:

    def begin(self, tab="\t"):
        self.code = []
        self.tab = tab
        self.level = 0

    def end(self):
        return "".join(self.code)

    def write(self, string):
        self.code.append(self.tab * self.level + string)

    def indent(self):
        self.level = self.level + 1

    def dedent(self):
        if self.level == 0:
            raise (SyntaxError, "internal error in code generator")
        self.level = self.level - 1


ussd_screens_dict = {'hello':
                         {"screen_msg": "Hi this is the hello screen\n1. Yes\n2. No",
                          "next_options": {"1": "yes", "2": "no"}},
                     'yes':
                         {"screen_msg": "Yes - the glass half full screen. See you later.",
                          "next_options": {}},
                     'no':
                         {"screen_msg": "No - the glass half empty screen. Good bye",
                          "next_options": {}},
                     'good':
                         {"screen_msg": "goooooood stuff!",
                          "next_options": {}}
                     }


def create_initiator_func_st_machine(ussd_dict):
    c = CodeGeneratorBackend()
    c.begin(tab="    ")
    c.write("\n")
    c.write("def session_initiator(current_session):\n")
    c.indent()
    c.write("pass\n")
    c.dedent()
    return c.end()


def create_new_function_machine(screen_name, single_screen_dict):
    screen_msg = single_screen_dict['screen_msg'].replace("\n","\\n")
    c = CodeGeneratorBackend()
    c.begin(tab="    ")
    c.write("\n")
    c.write("class " + screen_name + "():\n")
    c.indent()
    c.write("@staticmethod\n")
    c.write("@screen_renderer\n")
    c.write("def pre(current_session):\n")
    c.indent()
    c.write("return None\n")
    c.dedent()
    return c.end()


def create_new_function_renderer(screen_name, single_screen_dict):
    c = CodeGeneratorBackend()
    screen_msg = single_screen_dict['screen_msg'].replace("\n","\\n")
    c.begin(tab="    ")
    c.write("\n")
    c.write("@screen_renderer\n")
    c.write("def "+ screen_name + "(current_session):\n")
    c.indent()
    c.write('screen_msg = None\n')
    #c.write('screen_msg = "' + screen_msg + '"\n')
    c.write("next_options = None\n")
    #c.write("next_options = " + str(single_screen_dict['next_options']).replace(" u'", " '").replace("{u'","{'").replace("'",'"') + "\n")
    c.write("return screen_msg, next_options\n")
    c.dedent()
    return c.end()


def create_new_package_url_route(package_name,ussd_api):
    pre_url = "/at/long/" if ussd_api == "at_long" else "/at/short/"

    c = CodeGeneratorBackend()
    c.begin(tab="    ")
    c.write("@app.route('"+pre_url+package_name+"', methods=['POST', 'GET'])\n")
    c.write("def route_ussd_at_" + package_name + "():\n")
    c.indent()
    c.write("user_input, current_session = at_fetch_input_and_session(request, '" + ussd_api + "')\n")
    c.write("return ussd_at_" + package_name + "(current_session)\n")
    c.dedent()
    c.write("\n\n")
    c.write("@ussd_routed_req\n")
    c.write("def ussd_at_"+package_name+"(current_session):\n")
    c.indent()
    c.write("return st_mach_controller(current_session['user_input'], current_session, "+package_name+".st_machine)\n\n\n")
    c.dedent()
    return c.end()


def check_if_function_exists(file_name, func_name):
    with open(file_name) as f:
        for line in f:
            if line.startswith("def " + func_name + "(") or line.startswith("def " + func_name + " ("):
                print ("the function " + func_name + " already exists")
                #exit the for loop if the function is found
                return True
    print ("the function " + func_name + " does NOT exists")
    return False


def add_func_to_file(file_name, func_text):
    with open(file_name, 'a') as f:
        f.write("\n")
        f.write(func_text)


def write_machine_funcs_from_dict(ussd_dict):
    for key, value in ussd_dict.items():
        screen_name = key
        screen_dict = value
        st_machine_file = 'st_machine.py'
        if check_if_function_exists(st_machine_file, screen_name) == False:
            add_func_to_file(st_machine_file, create_new_function_machine(screen_name, screen_dict))
            print ("st_machine: added this new function: " + screen_name)


def write_renderer_funcs_from_dict(ussd_dict):
    for key, value in ussd_dict.items():
        screen_name = key
        screen_dict = value
        st_renderer_file = 'st_renderer.py'

        if check_if_function_exists(st_renderer_file, screen_name) == False:
            add_func_to_file(st_renderer_file, create_new_function_renderer(screen_name, screen_dict))
            print ("st_renderer: added this new function: " + screen_name)
#print json.dumps(ussd_screens_dict, sort_keys=False, indent=2, separators=(',', ': '))


def add_to_urls_route_file(package_name, ussd_api):
    url_funcs_to_insert = create_new_package_url_route(package_name,ussd_api)

    os.chdir(sys.path[0])  # change dir to dir of ussd_generator.py
    f = open("ussd_urls_routing.py", "r")
    contents = f.readlines()
    f.close()

    # insert the functions
    line_to_insert_function = None
    for index, value in enumerate(contents):
        if value.startswith('if __name__ == "__main__":'):
            line_to_insert_function = index
            break
    print ("inserting functions to ussd_urls_routing.py")
    contents.insert(line_to_insert_function, url_funcs_to_insert)


    # insert the import
    line_to_insert_import = 0
    for index, value in enumerate(contents):
        if value.startswith("import ") and value.endswith("st_machine\n"):
            line_to_insert_import = index + 1
            print ("line_to_insert_import="+str(line_to_insert_import))

    print ("inserting import to ussd_urls_routing.py")
    contents.insert(line_to_insert_import, "import "+package_name+".st_machine\n")

    line_of_funcs_dict = 0
    for index, value in enumerate(contents):
        if value.startswith("sc_ext = "):
            line_of_funcs_dict = index
            print ("line to update functions_dict = " + str(line_of_funcs_dict))

    funcs_dict_str = contents[line_of_funcs_dict].split("=")[1]
    next_number = len(funcs_dict_str.split(",")) + 1
    new_funcs_dict_line = contents[line_of_funcs_dict].split("}")[0] + ", '"+str(next_number)+"': ussd_at_"+package_name+"}\n\n"
    print (new_funcs_dict_line)
    del contents[line_of_funcs_dict]
    contents.insert(line_to_insert_function+1, new_funcs_dict_line)

    with open("ussd_urls_routing.py", "w") as f:
        f.writelines(contents)

def get_ussd_dict_from_file(json_file):
    with open(json_file) as f:
        file_text = f.read()
        #print str(file_text)
        return json.loads(file_text)


def add_ussd_journey_from_file(file_name, ussd_journey, action):
    os.chdir(sys.path[0] + "/" + ussd_journey)
    ussd_dict = get_ussd_dict_from_file(file_name)
    print ("PRINTING:  " + str(ussd_dict) + "PRINTED")

    if action in ('all','machine'):
        write_machine_funcs_from_dict(ussd_dict)

    if action in ('all', 'renderer'):
        print ("!! st_renderer.py depricated")
        # write_renderer_funcs_from_dict(ussd_dict)


def add_ussd_journey_from_api(json_obj, ussd_journey):
    os.chdir(sys.path[0] + "/" + ussd_journey)
    print ("journey: " + ussd_journey)

    print ("making it a dict from JSON")
    ussd_dict = json.loads(json_obj)

    print ("adding the missing funcs to st_machine.py")
    write_machine_funcs_from_dict(ussd_dict)

    print ("adding the missing screens to ussd_journey.json")
    json_obj_new = json.dumps(ussd_dict, sort_keys=False, indent=2, separators=(',', ': '))

    with open("ussd_journey.json", 'w') as f:
        f.write(json_obj_new)
    print ("finished with add!")





def reset_ussd_journey_from_file(file_name, ussd_journey, action):
    os.chdir(sys.path[0] + "/" + ussd_journey)
    ussd_dict = get_ussd_dict_from_file(file_name)

    if file_name != "ussd_journey.json":
        open("ussd_journey.json", "w").writelines([l for l in open(file_name).readlines()])

    if action in ('all','machine'):

        with open("st_machine.py", 'w') as f:
            f.write("''' This is the state machine '''"
                    "\nfrom ussd_core import screen_renderer\n")
            f.write(create_initiator_func_st_machine(ussd_dict))
        write_machine_funcs_from_dict(ussd_dict)

    if action in ('all', 'renderer'):
        print ("!! Not creating a renderer! -- This has been depricated!")
        #with open("st_renderer.py", 'w') as f:
        #    f.write("''' This is the state renderer file '''"
        #            "\nfrom ussd_core import screen_renderer\n")
        #write_renderer_funcs_from_dict(ussd_dict)



def create_ussd_package(package_name):
    initial_screen = "home"
    os.chdir(sys.path[0])  # change dir to dir of ussd_generator.py
    os.mkdir(sys.path[0] + "/" + package_name)  # create the directory
    default_journey = {initial_screen:
                           {"screen_msg": "Hello World!\n1. hello Again\n2.Hello Again x2\n3. Quit",
                            "next_options": {'1': initial_screen+"1",
                                             '2': initial_screen+"2",
                                             '3': "quit_screen"}},
                       initial_screen+"1":
                           {"screen_msg": "Hello World! Again\n1. Back\n2. Quit",
                            "next_options": {'1': initial_screen,
                                             '2': "quit_screen"}},
                       initial_screen + "2":
                           {"screen_msg": "Hello World!!! Again x2\n1. Back\n2. Quit",
                            "next_options": {'1': initial_screen,
                                             '2': "quit_screen"}},
                       "quit_screen":
                           {"screen_msg": "Good bye!",
                            "next_options": {}}
                       }

    os.chdir(sys.path[0] + "/" + package_name)  # change dir to dir of newly created package

    # create the files
    with open ("__init__.py", "w") as f:
        f.write("")

    # this needs to be created before the st_machine.py & st_renderer.py files
    with open("ussd_journey.json", "w") as f:
        f.write(json.dumps(default_journey, sort_keys=False, indent=2, separators=(',', ': ')))

    # creates the st_machine.py file
    reset_ussd_journey_from_file("ussd_journey.json", package_name, 'machine')

    # creates the functions & the imports required in the ussd_urls_routing.py
    add_to_urls_route_file(package_name, "at_long")


if __name__ == '__main__':

    ''' -a --action,  -j --json_file, -u --ussd_journey, '''

    action = None # sys.argv[-2]
    ussd_journey = None # sys.argv[-1]
    initial_screen = None # sys.argv[-3].split("=")[1] if sys.argv[-3].startswith("--initial_screen=") else False
    json_file = None

    opts, args = getopt.getopt(sys.argv[1:], "ha:u:i:j:", ["help", "action=", "ussd_journey=", "initial_screen=", "json_file="])

    for opt, arg in opts:
        if opt in ("-h", "--help"):

            print ("-a --action  {add|add-machine|add-renderer|reset-renderer|create_journey}")
            print ("-j --json_file the full file path of the json_file that hosts the journey e.g. /home/name/my_journey.json")
            print ("-u --ussd_journey the package name")
            exit()

        elif opt in ("-a", "--action"):
            action = arg

        elif opt in ("-u", "--ussd_journey"):
            ussd_journey = arg

        elif opt in ("-i", "--initial_screen"):
            initial_screen = arg

        elif opt in ("-j", "--json_file"):
            json_file = arg
        print ("opt = " + opt)
        print ("arg = " + arg)

    if ussd_journey is None:
        print ("ussd_journey name is missing")
        exit()
    if action is None:
        print ("action is missing")
        exit()
    if action == "create_package" and json_file is None:
        print ("json file is missing")
        exit()
    elif action == "create_package":
        if not os.path.isfile(json_file):
            print ("json file is missing")


    # get list of directories (ussd_journeys)
    os.chdir(sys.path[0])  # change dir to dir of ussd_generator.py
    dirs = next(os.walk('.'))[1]
    i = 0
    while i < len(dirs):
        dir_name = dirs[i]
        i += 1
        if dir_name.startswith("."):
            dirs.remove(dir_name)
            i -= 1

    if os.path.isdir(sys.path[0] + "/" + ussd_journey):
        if json_file is not None and os.path.isfile(json_file):
            base_json_file = json_file
        else:
            base_json_file = "ussd_journey.json"

        if action in ('add-all', 'add'):
            add_ussd_journey_from_file(base_json_file, ussd_journey, 'all')


        elif action in ('reset','reset-all'):
            reset_ussd_journey_from_file(base_json_file, ussd_journey, 'all')


        else:
            print ("USAGE: python " \
                  + os.path.realpath(__file__) \
                  + " -a {add|reset} -j </path/to/ussd_journey.json> -u <ussd_journey_name>")

            print ("EXAMPLE:")
            print ("python " \
                  + os.path.realpath(__file__) + " -a add -u example")


            print ("EXAMPLE:")
            print ("python " \
                  + os.path.realpath(__file__) + " -a reset -j /path/to/ussd_journey.json -u example")

    elif action == 'create_package':
        create_ussd_package(ussd_journey)

        if json_file is not None:
            reset_ussd_journey_from_file(json_file, ussd_journey, 'all', initial_screen)

        else:

            print ("USAGE: python " \
                  + os.path.realpath(__file__) \
                  + " --action create_package --ussd_journey <package_name>")

            print ("python " \
                  + os.path.realpath(__file__) + " --action create_package --ussd_journey example1")

    else:
        print ("ussd journey package does NOT exist: " + ussd_journey)
        print (str(len(sys.argv)))
        print ("the known packages include: " + ','.join([str(x) for x in dirs]))